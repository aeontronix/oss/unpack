package com.aeontronix.unpack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class FSSourceFile extends SourceFile {
    private final File file;

    protected FSSourceFile(File file, String path) {
        super(path);
        this.file = file;
    }

    @Override
    protected InputStream createInputStream() throws UnpackException {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new UnpackException(e.getMessage(), e);
        }
    }
}
