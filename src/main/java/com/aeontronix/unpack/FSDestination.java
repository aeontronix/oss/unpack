package com.aeontronix.unpack;

import com.aeontronix.commons.file.FileUtils;
import com.aeontronix.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FSDestination extends Destination {
    private File file;

    public FSDestination(File file) throws UnpackException {
        this.file = file;
        try {
            FileUtils.mkdirs(file);
        } catch (IOException e) {
            throw new UnpackException(e.getMessage(), e);
        }
    }

    @Override
    public void write(Source source) throws UnpackException {
        source.sort();
        super.write(source);
    }

    @Override
    public void write(UFile sourceFile) throws UnpackException {
        try {
            String destPath = sourceFile.getDestPath();
            if (File.separatorChar != '/') {
                destPath = destPath.replace('/', File.separatorChar);
            }
            File destFile = new File(file.getAbsolutePath() + File.separator + destPath);
            final File parentFile = destFile.getParentFile();
            if (!parentFile.exists()) {
                FileUtils.mkdirs(parentFile);
            }
            if (sourceFile instanceof SourceDirectory) {
                FileUtils.mkdir(destFile);
            } else if (sourceFile instanceof SourceFile) {
                try (FileOutputStream os = new FileOutputStream(destFile)) {
                    final byte[] data = ((SourceFile) sourceFile).getData();
                    if (data != null) {
                        os.write(data);
                    } else {
                        try (InputStream is = ((SourceFile) sourceFile).createInputStream()) {
                            IOUtils.copy(is, os);
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw new UnpackException(e.getMessage(), e);
        }
    }
}
