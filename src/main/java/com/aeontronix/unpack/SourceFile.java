package com.aeontronix.unpack;

import com.aeontronix.unpack.transformer.Transformer;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class SourceFile extends UFile {
    protected byte[] data;
    protected ArrayList<Transformer> transformers;

    protected SourceFile(String path) {
        super(path);
    }

    protected abstract InputStream createInputStream() throws UnpackException;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public synchronized List<Transformer> getTransformers() {
        if (transformers == null) {
            return Collections.emptyList();
        } else {
            return Collections.unmodifiableList(transformers);
        }
    }

    public synchronized void addTransformer(Transformer transformer) {
        if (transformers == null) {
            transformers = new ArrayList<>();
        }
        transformers.add(transformer);
    }
}
