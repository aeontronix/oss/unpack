package com.aeontronix.unpack;

import com.aeontronix.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;

public class ZipSource extends Source {
    private final File file;
    private final String extension;
    private java.util.zip.ZipFile zipFile;

    public ZipSource(File file, String extension) {
        this.file = file;
        this.extension = extension;
    }

    @Override
    public void read() throws UnpackException {
        try {
            zipFile = new java.util.zip.ZipFile(file);
            java.util.zip.ZipFile zipFile = new java.util.zip.ZipFile(file);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                add(new ZipFile(zipFile, entries.nextElement()));
            }
        } catch (IOException e) {
            throw new UnpackException(e.getMessage(), e);
        }
    }

    @Override
    public void close() throws IOException {
        IOUtils.close(zipFile);
    }
}
