package com.aeontronix.unpack;

import org.jetbrains.annotations.NotNull;

public abstract class UFile {
    protected String path;
    protected String destPath;

    public UFile(@NotNull String path) {
        this.path = path;
        this.destPath = path;
    }

    public String getName() {
        final int idx = path.indexOf('/');
        if (idx == -1) {
            return path;
        } else {
            return path.substring(idx + 1);
        }
    }

    public String getPath() {
        return path;
    }

    public String getDestPath() {
        return destPath;
    }

    public void setDestPath(String destPath) {
        if (!this.destPath.equals(path) && !this.destPath.equals(destPath)) {
            throw new IllegalStateException("destPath has already been changed from " + path + " to " + this.destPath + " cannot change to " + destPath);
        }
        this.destPath = destPath;
    }
}
