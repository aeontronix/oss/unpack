package com.aeontronix.unpack;

import com.aeontronix.commons.io.IOUtils;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipDestination extends Destination {
    private FileOutputStream fos;
    private ZipOutputStream os;

    public ZipDestination(File file) throws UnpackException {
        try {
            fos = new FileOutputStream(file);
            os = new ZipOutputStream(fos);
        } catch (FileNotFoundException e) {
            throw new UnpackException(e.getMessage(), e);
        }
    }

    @Override
    public void write(UFile sourceFile) throws UnpackException {
        try {
            if (sourceFile instanceof SourceFile) {
                ZipEntry zipEntry = new ZipEntry(sourceFile.getDestPath());
                os.putNextEntry(zipEntry);
                final byte[] data = ((SourceFile) sourceFile).getData();
                if (data != null) {
                    os.write(data);
                } else {
                    try (InputStream sis = ((SourceFile) sourceFile).createInputStream()) {
                        IOUtils.copy(sis, os);
                    }
                }
                os.closeEntry();
            }
        } catch (IOException e) {
            throw new UnpackException(e);
        }
    }

    @Override
    public void close() throws IOException {
        IOUtils.close(os, fos);
    }
}
