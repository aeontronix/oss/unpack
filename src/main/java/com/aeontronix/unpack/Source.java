package com.aeontronix.unpack;

import com.aeontronix.commons.SortUtils;
import com.aeontronix.commons.TopologicalSortComparator;
import com.aeontronix.commons.exception.CircularDependencyException;
import com.aeontronix.commons.exception.UnexpectedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class Source implements Closeable {
    protected List<UFile> files = new ArrayList<>();
    protected LinkedHashMap<String, UFile> filesIdx = new LinkedHashMap<>();

    public abstract void read() throws UnpackException;

    public List<UFile> getFiles() {
        return files;
    }

    public static Source create(File file, FileType fileType) throws UnpackException {
        switch (fileType) {
            case DIR:
                return new FSSource(file);
            case ZIP:
                return new ZipSource(file, fileType.getExtension());
            default:
                throw new UnexpectedException("Unexpected source type: " + fileType);
        }
    }

    @Override
    public void close() throws IOException {
    }

    public void add(UFile file) {
        files.add(file);
        filesIdx.put(file.getPath(), file);
    }

    public void sort() {
        try {
            files = SortUtils.topologicalSort(files, new TopologicalSortComparator<UFile>() {
                @Override
                public SortUtils.TopologicalSortRelationship getRelationship(UFile source, UFile target) {
                    return source.getPath().startsWith(target.getPath()) ? SortUtils.TopologicalSortRelationship.STRONG : SortUtils.TopologicalSortRelationship.NONE;
                }

                @Override
                public String getObjectRepresentation(UFile object) {
                    return object.getPath();
                }
            });
        } catch (CircularDependencyException e) {
            throw new UnexpectedException(e.getMessage(), e);
        }
    }

    @Nullable
    public UFile getFile(@NotNull String path) {
        return filesIdx.get(path);
    }

    private class UFileWrapper {
        private UFile file;
        private UFileWrapper parent;
    }
}
