package com.aeontronix.unpack;

import com.aeontronix.commons.io.IOUtils;
import com.aeontronix.unpack.transformer.FileMatcher;
import com.aeontronix.unpack.transformer.Matcher;
import com.aeontronix.unpack.transformer.Transformer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Unpacker {
    private final List<Transformer> transformers = new ArrayList<>();
    private Source source;
    private Destination destination;

    public Unpacker(File file, FileType fileType, File destination, FileType destinationType) throws UnpackException {
        this.source = Source.create(file, fileType);
        this.destination = Destination.create(destination, destinationType);
    }

    public void unpack() throws UnpackException {
        try {
            source.read();
            for (Transformer transformer : transformers) {
                final Matcher matcher = transformer.getMatcher();
                if (matcher instanceof FileMatcher) {
                    final String path = ((FileMatcher) matcher).getPath();
                    UFile file = source.getFile(path);
                    if (file == null) {
                        switch (((FileMatcher) matcher).getRequired()) {
                            case REQUIRED:
                                throw new UnpackException("File not found: " + path);
                            case CREATE:
                                file = new InMemSourceFile(path, null);
                                source.add(file);
                        }
                    }
                    if (file != null) {
                        if (file instanceof SourceFile) {
                            ((SourceFile) file).addTransformer(transformer);
                        } else {
                            throw new IllegalStateException(path + " isn't a file");
                        }
                    }
                }
            }
            for (UFile file : source.getFiles()) {
                if (file instanceof SourceFile) {
                    final SourceFile sFile = (SourceFile) file;
                    for (Transformer transformer : sFile.getTransformers()) {
                        if (sFile.getData() == null) {
                            try (InputStream is = sFile.createInputStream()) {
                                if (is != null) {
                                    sFile.setData(IOUtils.toByteArray(is));
                                }
                            }
                        }
                        try {
                            final byte[] result = transformer.transform(sFile);
                            if (result != null) {
                                sFile.setData(result);
                            }
                        } catch (Exception e) {
                            throw new UnpackException(e);
                        }
                    }
                }
            }
            destination.write(source);
        } catch (IOException e) {
            throw new UnpackException(e);
        } finally {
            IOUtils.close(source, destination);
        }
    }

    public void addTransformer(Transformer transformer) {
        transformers.add(transformer);
    }

    public void addTransformers(Collection<Transformer> transformers) {
        this.transformers.addAll(transformers);
    }

    public void addTransformers(Transformer... transformers) {
        if (transformers != null) {
            this.transformers.addAll(Arrays.asList(transformers));
        }
    }
}
