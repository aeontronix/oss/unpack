package com.aeontronix.unpack;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;

public class ZipFile extends SourceFile {
    private final java.util.zip.ZipFile zipFile;
    private final ZipEntry archiveEntry;

    public ZipFile(java.util.zip.ZipFile zipFile, ZipEntry archiveEntry) {
        super(archiveEntry.getName());
        this.zipFile = zipFile;
        this.archiveEntry = archiveEntry;
    }

    @Override
    protected InputStream createInputStream() throws UnpackException {
        try {
            return zipFile.getInputStream(archiveEntry);
        } catch (IOException e) {
            throw new UnpackException(e);
        }
    }
}
