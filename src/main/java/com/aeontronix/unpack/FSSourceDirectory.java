package com.aeontronix.unpack;

import java.io.File;

public class FSSourceDirectory extends SourceDirectory {
    private final File file;

    public FSSourceDirectory(File file, String path) {
        super(path);
        this.file = file;
    }

    @Override
    public String getName() {
        return file.getName();
    }
}
