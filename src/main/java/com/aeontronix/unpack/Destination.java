package com.aeontronix.unpack;

import com.aeontronix.commons.exception.UnexpectedException;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

public abstract class Destination implements Closeable {
    public static Destination create(File file, FileType fileType) throws UnpackException {
        switch (fileType) {
            case DIR:
                return new FSDestination(file);
            case ZIP:
                return new ZipDestination(file);
            default:
                throw new UnexpectedException("Unexpected source type: " + fileType);
        }
    }

    public void write(Source source) throws UnpackException {
        for (UFile sourceFile : source.getFiles()) {
            write(sourceFile);
        }
    }

    public abstract void write(UFile sourceFile) throws UnpackException;

    @Override
    public void close() throws IOException {
    }
}
