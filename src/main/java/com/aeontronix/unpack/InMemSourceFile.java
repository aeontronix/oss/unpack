package com.aeontronix.unpack;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class InMemSourceFile extends SourceFile {
    public InMemSourceFile(String path, byte[] data) {
        super(path);
        setData(data);
    }

    @Override
    protected InputStream createInputStream() throws UnpackException {
        return data != null ? new ByteArrayInputStream(data) : null;
    }
}
