package com.aeontronix.unpack.transformer;

import com.aeontronix.commons.Required;

public class FileMatcher implements Matcher {
    private String path;
    private Required required;

    public FileMatcher(String path, Required required) {
        this.path = path;
        this.required = required;
    }

    public String getPath() {
        return path;
    }

    public Required getRequired() {
        return required;
    }
}
