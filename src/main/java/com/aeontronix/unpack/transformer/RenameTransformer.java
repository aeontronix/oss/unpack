package com.aeontronix.unpack.transformer;


import com.aeontronix.commons.Required;
import com.aeontronix.unpack.SourceFile;

public class RenameTransformer extends Transformer {
    private String newPath;

    public RenameTransformer(String path, String newPath) {
        this(path, newPath, false);
    }

    public RenameTransformer(String path, String newPath, boolean skipIfAbsent) {
        super(new FileMatcher(path, skipIfAbsent ? Required.OPTIONAL : Required.REQUIRED));
        this.newPath = newPath;
    }

    @Override
    public byte[] transform(SourceFile file) throws Exception {
        file.setDestPath(newPath);
        return null;
    }
}
