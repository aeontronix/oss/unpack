package com.aeontronix.unpack.transformer;

import com.aeontronix.commons.Required;
import com.aeontronix.commons.xml.XmlUtils;
import com.aeontronix.unpack.SourceFile;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;

public abstract class XMLTransformer extends Transformer {
    private boolean namespaceAware;
    private boolean xmlDeclaration;

    public XMLTransformer(String path, Required required, boolean namespaceAware, boolean xmlDeclaration) {
        super(path, required);
        this.namespaceAware = namespaceAware;
        this.xmlDeclaration = xmlDeclaration;
    }

    public XMLTransformer(Matcher matcher, boolean namespaceAware, boolean xmlDeclaration) {
        super(matcher);
        this.namespaceAware = namespaceAware;
        this.xmlDeclaration = xmlDeclaration;
    }

    @Override
    public final byte[] transform(SourceFile file) throws Exception {
        final Document document;
        if (file.getData() != null) {
            document = XmlUtils.parse(file.getData());
        } else {
            document = XmlUtils.createDocument(namespaceAware);
        }
        transform(document);
        final ByteArrayOutputStream tmp = new ByteArrayOutputStream();
        XmlUtils.serialize(document, tmp, xmlDeclaration, false);
        tmp.flush();
        tmp.close();
        return tmp.toByteArray();
    }

    public abstract void transform(Document xmlDoc) throws Exception;
}
