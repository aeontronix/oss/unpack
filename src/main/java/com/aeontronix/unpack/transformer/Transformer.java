package com.aeontronix.unpack.transformer;

import com.aeontronix.commons.Required;
import com.aeontronix.unpack.SourceFile;

public abstract class Transformer {
    private Matcher matcher;

    public Transformer(String path, Required required) {
        this(new FileMatcher(path, required));
    }

    public Transformer(Matcher matcher) {
        this.matcher = matcher;
    }

    public Matcher getMatcher() {
        return matcher;
    }

    public abstract byte[] transform(SourceFile file) throws Exception;
}
