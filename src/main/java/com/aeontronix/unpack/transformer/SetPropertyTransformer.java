package com.aeontronix.unpack.transformer;

import com.aeontronix.commons.Required;
import com.aeontronix.unpack.SourceFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Properties;

public class SetPropertyTransformer extends Transformer {
    private final HashMap<String, String> properties = new HashMap<>();

    public SetPropertyTransformer(String path, Required required, HashMap<String, String> properties) {
        super(path, required);
        this.properties.putAll(properties);
    }

    public SetPropertyTransformer(Matcher matcher, HashMap<String, String> properties) {
        super(matcher);
        this.properties.putAll(properties);
    }

    public SetPropertyTransformer(Matcher matcher, String propertyKey, String propertyValue) {
        super(matcher);
        this.properties.put(propertyKey, propertyValue);
    }

    @Override
    public byte[] transform(SourceFile file) throws Exception {
        Properties p = new Properties();
        if (file.getData() != null) {
            p.load(new ByteArrayInputStream(file.getData()));
        }
        p.putAll(properties);
        final ByteArrayOutputStream result = new ByteArrayOutputStream();
        p.store(result, null);
        result.flush();
        result.close();
        return result.toByteArray();
    }
}
