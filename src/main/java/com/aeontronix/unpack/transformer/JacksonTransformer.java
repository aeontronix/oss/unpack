package com.aeontronix.unpack.transformer;

import com.aeontronix.commons.Required;
import com.aeontronix.unpack.SourceFile;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

public abstract class JacksonTransformer<X extends ObjectNode> extends Transformer {
    protected final ObjectMapper objectMapper;
    protected Class<X> classType;


    public JacksonTransformer(Matcher matcher, Class<X> classType, ObjectMapper objectMapper) {
        super(matcher);
        this.classType = classType;
        this.objectMapper = objectMapper;
    }

    public JacksonTransformer(Matcher matcher, Class<X> classType) {
        this(matcher, classType, new ObjectMapper());
    }

    public JacksonTransformer(String path, Required required, Class<X> classType, ObjectMapper objectMapper) {
        this(new FileMatcher(path, required), classType, objectMapper);
    }

    public JacksonTransformer(String path, Required required, Class<X> classType) {
        this(new FileMatcher(path, required), classType, new ObjectMapper());
    }

    private static void checkIsClass(String name, Class<? extends JsonNode> clazz, JsonNode node) {
        if (node != null && !clazz.isInstance(node)) {
            throw new IllegalArgumentException("node " + name + " isn't of type " + clazz.getSimpleName() + " but instead is " + node.getClass().getSimpleName());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public final byte[] transform(SourceFile file) throws Exception {
        final JsonNode result = transform((X) (file.getData() != null ? objectMapper.readTree(file.getData()) : null));
        return objectMapper.writeValueAsBytes(result);
    }

    public abstract JsonNode transform(X node) throws Exception;

    public ObjectNode getObject(ObjectNode node, String name, boolean mandatory) {
        final JsonNode obj = node.get(name);
        checkIsClass(name, ObjectNode.class, obj);
        if (obj == null && mandatory) {
            throw new IllegalArgumentException("node " + name + " is missing");
        }
        return (ObjectNode) obj;
    }

    @NotNull
    public ObjectNode getOrCreateObject(ObjectNode node, String name) throws IllegalArgumentException {
        JsonNode obj = node.get(name);
        checkIsClass(name, ObjectNode.class, obj);
        if (obj == null) {
            obj = objectMapper.createObjectNode();
            node.set(name, obj);
        }
        return (ObjectNode) obj;
    }

    public ArrayNode getArray(ObjectNode node, String name, boolean mandatory) {
        final JsonNode obj = node.get(name);
        checkIsClass(name, ArrayNode.class, obj);
        if (obj == null && mandatory) {
            throw new IllegalArgumentException("node " + name + " is missing");
        }
        return (ArrayNode) obj;
    }

    @NotNull
    public ArrayNode getOrCreateArray(ObjectNode node, String name) throws IllegalArgumentException {
        JsonNode obj = node.get(name);
        checkIsClass(name, ArrayNode.class, obj);
        if (obj == null) {
            obj = objectMapper.createArrayNode();
            node.set(name, obj);
        }
        return (ArrayNode) obj;
    }

    public <Z> List<Z> toList(ArrayNode arrayNode, Class<Z> clz) throws IOException {
        return objectMapper.readerForListOf(clz).readValue(arrayNode);
    }

    public boolean contains(ArrayNode arrayNode, String str) {
        if (arrayNode != null) {
            for (JsonNode obj : arrayNode) {
                if (obj.asText().equals(str)) {
                    return true;
                }
            }
        }
        return false;
    }
}
